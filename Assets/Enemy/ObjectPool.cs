﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] int poolSize = 5;
    [SerializeField] float spawnTimer = 1f;

    // Holds all of our instantiated objects. This way we don't initialize and destroy enemies continuously, which is great for performances
    GameObject[] pool; 

    void Awake() 
    {
        // We populate the pool in Awake to make sure that, if any other script is going to need the pool objs later on, everything will be ready by the time the try grab them.
        PopulatePool();
    }

    void Start()
    {
        StartCoroutine(SpawnEnemies());
    }

    // Instantiates all the enemies at once. This is called only once per game, great for performances
    void PopulatePool()
    {
        pool = new GameObject[poolSize];
        for (int i = 0; i < poolSize; ++i)
        {
            // Overload of Instantiate that only requires the GameObject to spawn and the parent transform. The parent is GamePool and, since this script is attached to GamePool itself, the parent transform is just transform.
            // We don't specify position and rotation because they're handled by the EnemyMover.cs script
            pool[i] = Instantiate(enemyPrefab, transform);
            pool[i].SetActive(false);
        }
    }

    void EnableObjectInPool()
    {
        foreach (GameObject enemy in pool)
        {
            if (enemy.activeInHierarchy) { continue; } // Is the enemy active in the Scene?
            enemy.SetActive(true); // This also resets enemy health and path, see OnEnable in EnemyMover and EnemyHealth
            return;
        }
    }

    IEnumerator SpawnEnemies()
    {
        while (true)
        {
            EnableObjectInPool();
            yield return new WaitForSeconds(spawnTimer);
        }
    }
}
