﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    [SerializeField] [Range(0f, 5f)] float speed = 1f;

    List<Waypoint> path = new List<Waypoint>();

    void OnEnable() // Called after Awake and whenever SetActive(true) is used.
    {
        FindPath();
        ReturnToStart();
        StartCoroutine(FollowPath()); // To invoke a coroutine you need to wrap it within StartCoroutine()
    }

    void FindPath()
    {
        path.Clear(); // Clears the list to not append multiple paths

        // The empty GameObject named Path has a "Path" tag
        GameObject waypoints = GameObject.FindGameObjectWithTag("Path");
        foreach (Transform waypoint in waypoints.transform) // We loop through the Path children and add them
        {
            path.Add(waypoint.GetComponent<Waypoint>());
        }
    }

    void ReturnToStart()
    {
        transform.position = path[0].transform.position;
    }

    IEnumerator FollowPath()
    {
        foreach (Waypoint waypoint in path)
        {
            Vector3 startPosition = transform.position; // parent not needed, these scripts are on their root object
            Vector3 endPosition = waypoint.transform.position;
            float travelPercent = 0f;

            transform.LookAt(waypoint.transform); // So that the enemy faces the direction of travel

            while (travelPercent < 1f)
            {
                travelPercent += Time.deltaTime * speed;
                transform.position = Vector3.Lerp(startPosition, endPosition, travelPercent); // smooth movement
                yield return new WaitForEndOfFrame(); // The coroutine steps every frame
            }
        }

        // The enemy arrived at the end of the path: disable it and move it to the start of the path. We don't destroy it to improve performances, see ObjectPool.cs
        gameObject.SetActive(false);
    }
}
