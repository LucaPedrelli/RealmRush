﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] int maxHitPoints = 3;
    int currentHitPoints;

    void OnEnable() // Called after Awake and whenever SetActive(true) is used.
    {
        currentHitPoints = maxHitPoints;
    }

    void OnParticleCollision(GameObject other) // Only works if particle system has "Send Collision Message" to true
    { 
        ProcessHit();
    }

    void ProcessHit()
    {
        currentHitPoints--;
        if (currentHitPoints <= 0)
        {
            // The enemy died: disable it and move it to the start of the path. We don't destroy it to improve performances, see ObjectPool.cs
            gameObject.SetActive(false);
        }
    }
}
